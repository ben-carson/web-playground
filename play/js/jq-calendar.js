$(document).ready( function() {
	//for the datepicker
	$("#basicCal").datepicker( {
		dateFormat: 'DD, d MM, yy',
		onSelect: function(dateText, instanceOfDatePicker) {
			$("#selectedDate").text(dateText);
		},
		showOn: 'button',
		buttonImage: '../images/calendar_2.ico',
		buttonImageOnly: 'true'
	});
	
	//for fullcalendar-1.5.1
	$("#fullCalendarDiv").fullCalendar( {
		height: 250,
		dayClick: function(date, allDay, jsEvent, view) {
			var f_d = $.fullCalendar.formatDate(date,'MMMM dd yyyy');
			var displayStr = 'Selected date: '+f_d;
			displayStr += view.name+' '+view.title;
			$("#fullCalDisplay").text( function(index, text) {
				var oldDate = text;
				if(oldDate!='') {
					alert('Changing '+oldDate+' to '+f_d);
				} else {
					alert('first Date: ' + f_d);
				}
				$(this).text(displayStr);
			});
		}
	});
	
	//for jMonthCalendar 1.3.2-beta2
	$.jMonthCalendar.Initialize(
		{
		containerId: '#jMonthCalendarDiv',
		headerHeight: 50,
		firstDayOfWeek: 0
		}
		, null);
});

