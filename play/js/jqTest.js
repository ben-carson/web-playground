$(document).ready( function() {
	$("#acctNumSection").load('./includes/acctNum.html', function() {
		$("#jqSubmit").click(refreshAcctNumber);
	});

	$("#jqTestForm select[id*=contact]").change(refreshLvl1Dropdown);
	$("#lvl1>select").change(refreshLvl2Dropdown);
	$("#lvl2>select").change(refreshLvl3Dropdown);
	$("#lvl3>select").change(refreshDescription);
	$("#descField").keyup(refreshSubmit);
	$("#ticketSubmit").click(validateAndSubmit);
});

function refreshAcctNumber() {
	//get the input field from form that contains the word 'siteId' in its id attribute
	var siteIdData = $("#jqTestForm input[id*=siteId]");
	var siteIdId = siteIdData.attr('name');
	var siteIdValue = siteIdData.val();
	var jqXHRObj = $.ajax({
		type: "post",
		url: "./includes/acctNum.html",
		data: $("#jqTestForm").serialize(),
//		data: siteIdId+"="+siteIdValue,
		dataType: "html",
		beforeSend: function() {
			$("#acctNumSection").addClass('loading');
		},
		success: function(html) {
			$("#acctNumSection").removeClass('loading');
			$("#acctNumSection").html(html);
			refreshContacts();		//trigger the cascade refresh...
		},
		error: function(error) {
			$("#siteIdStatus").removeClass('loading');
			$("#siteIdStatus").html("An error occurred:" + error);
			$("#siteIdStatus").addClass('badMessage');
		}
	});
	$("#jqTestForm input[id*=siteId]").attr("disabled",true);  //FIXME: only for testing purposes.
}

function refreshContacts() {
	var siteIdData = $("#jqTestForm input[id*=siteId]");
	var siteIdId = siteIdData.attr('name');
	var siteIdValue = siteIdData.val();
	var contactData = $("#jqTestForm select[id*=contact]");
	if(siteIdData.is(':disabled')) {  //account was successfully retrieved.
		var jqXHRObj = $.ajax({
			type: "post",
			url: "./includes/contacts.html",
			data: siteIdId + "=" + siteIdValue,
			//data: $("#jqTestForm").serialize(),
			dataType: "html",
			beforeSend: function() {
				$('#contactSection').html("<img src='./images/loader.gif'></img>");
				$('#contactSection img').show("fast");
			},
			success: function(returnedHtml) {
				$('#contactSection img').hide("slow");
				$('#contactSection').html(returnedHtml);
			},
			error: function(returnedError) {
				$('#contactSection img').hide("slow");
				$('#contactSection').html("An error occurred:" + returnedError);
			}
		});
	} else {
		contactData.attr("disabled",true);
		contactData.emptySelect();
		refreshLvl1Dropdown();		//trigger the cascade refresh...
	}
}

function refreshLvl1Dropdown() {
	var contactValue = $("#jqTestForm select[id*=contact]").val();
	var lvl1Field = $("#lvl1>select");
	if(contactValue != null && contactValue != '') {
		alert("refreshLvl1Dropdown not yet implemented!");
		lvl2Field.attr("disabled",false);
	} else {
		lvl1Field.attr("disabled",true);
		lvl1Field.emptySelect();
		refreshLvl2Dropdown();		//trigger the cascade refresh...
	}
}
function refreshLvl2Dropdown() {
	var lvl1Value = $("#lvl1>select").val();
	var lvl2Field = $("#lvl2>select");
	if(lvl1Value != null && lvl1Value != '') {
		alert("refreshLvl2Dropdown not yet implemented!");
		lvl2Field.attr("disabled",false);
	} else {
		lvl2Field.attr("disabled",true);
		lvl2Field.emptySelect();
		refreshLvl3Dropdown();
	}
}
function refreshLvl3Dropdown() {
	var lvl2Value = $("#lvl2>select").val();
	var lvl3Field = $("#lvl3>select");
	if(lvl2Value != null && lvl2Value != '') {
		alert("refreshLvl3Dropdown not yet implemented!");
		lvl2Field.attr("disabled",false);
	} else {
		lvl3Field.attr("disabled",true);
		lvl3Field.emptySelect();
		refreshDescription();
	}
}
function refreshDescription() {
	var lvl3Value = $("#lvl3>select").val();
	var descField = $("#descField");
	if(lvl3Value != null && lvl3Value != '') {
		alert("refreshDescription not yet implemented!");
		lvl3Field.attr("disabled",false);
	} else {
		descField.attr("disabled",true);
		refreshSubmit();
	}
}
function refreshSubmit() {
	var descValue = $("#descField").val();
	var subButton = $("#ticketSubmit");
	if(descValue != null && descValue != '') {
		alert("refreshSubmit not yet implemented!");
		subButton.attr("disabled",false);
	} else {
		subButton.attr("disabled",true);
	}
}
function validateAndSubmit() {
	alert("validateAndSubmit not yet implemented!");
}

