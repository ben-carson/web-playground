/*
 * Since the Sprite menu is our fallback state when JavaScript is disabled (if we�ve done it right), we�d better get rid of those CSS-applied   * background images on hover, because we�ll create our own in the script below
 */

$(document).ready( function() {
	$(".nav").children("li").each(function() {
		var current = "nav current-" + ($(this).attr("class"));
		var parentClass = $(".nav").attr("class");
		//watch to NOT remove the background of the currently selected element,
		// we only want to remove the backgrounds for hover events
		if (parentClass != current) { 
			//remove css-based background, since javascript is enabled
			$(this).children("a").css({backgroundImage:"none"});
		}
	});
	
	//create events for each nav item
	attachNavEvents(".nav", "home");
	attachNavEvents(".nav", "about");
	attachNavEvents(".nav", "services");
	attachNavEvents(".nav", "contact");

	function attachNavEvents(parent, myClass) {
		$(parent + " ." + myClass).mouseover(function() {
			$(this).append('<div class="nav-' + myClass + '"></div>');
			$("div.nav-" + myClass)
				.css({display:"none"})
				.fadeIn(200);
		}).mouseout(function() {
			//fade out & destroy pseudo link
			$("div.nav-" + myClass).fadeOut(200, function() {
				$(this).remove();
			});
		});
	}
	
});