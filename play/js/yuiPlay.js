YAHOO.namespace("example.calendar");

YAHOO.example.calendar.launchCal = function() {
	//create the calendar object, specifying the container
	var myCal = new YAHOO.widget.Calendar("bxCal");
	//draw the calendar on-screen
	myCal.render();
	//hide it straight away
	myCal.hide();
	
	//define the showCal function which shows the calendar
	var showCal = function() {
		myCal.show();
	}
	
	//when the calender icon, calicon, is clicked, display calendar widget
	YAHOO.util.Event.addListener("calicon", "click", showCal);

};

//load the launchCal function once the DOM is fully loaded and the button is clicked
YAHOO.util.Event.onDOMReady(YAHOO.example.calendar.launchCal);

