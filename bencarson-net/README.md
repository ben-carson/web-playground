# [bencarson.net](http://bencarson.net)
=============

My personal website. Basically a collection of links to other services and records of my work.

## Features

## Project information

* Source: https://github.com/ben-carson/bencarson.net.git
* Web: http://bencarson.net
* Twitter: http://twitter.com/catsandcode
* Version: 0.3


## License

MIT 

### Major components:

* YUI: BSD License

### Everything else:

